package com.concrete.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import twitter4j.DirectMessage;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

@RestController
@RequestMapping("api/v1/bot/")
public class BotController {


	private static Twitter twitter = TwitterFactory.getSingleton();

	@RequestMapping(value="status", method=RequestMethod.GET)
	public String test() throws TwitterException {

		Status updateStatus = twitter.updateStatus("teste");

		return updateStatus.toString();
	}

	@RequestMapping(value="query/{question}", method=RequestMethod.GET)
	public String query(@PathVariable String question) throws TwitterException {

		Query query = new Query(question);

		QueryResult result = twitter.search(query);

		return result.getTweets().get(0).getText();
	}

	@RequestMapping(value="send", method=RequestMethod.GET)
	public String send() throws TwitterException {
		try{
			DirectMessage result = twitter.sendDirectMessage("rafael9s9", "text");
		}catch (Exception e) {
			System.out.println("ff");
		}try{
			DirectMessage result2 = twitter.sendDirectMessage(542012920, "text33");
		}catch (Exception e) {
			System.out.println("ffds");
		}
		return "";
	}
}
