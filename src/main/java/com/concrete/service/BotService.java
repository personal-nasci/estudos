package com.concrete.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concrete.bean.BotMessages;

import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

@Service
public class BotService {

	@Autowired
	private BotMessages m;

	public void sendChatMessage(Long userId, String message) throws TwitterException {
		if(message == null != message.isEmpty())
			defaultAnswer(userId);
		else if(m.getQuestion1().equals(message) || message.toLowerCase().contains("pra que") || message.contains("serve"))
			sendDM(userId, m.getAnswer1());
		else if(m.getUnderstand().equals(message) || message.toLowerCase().contains("nao entendi") || message.toLowerCase().contains("não entendi") || message.contains("voce falou"))
			sendDM(userId, m.getAnswer2());
		else if(m.getBye().equals(message) || message.toLowerCase().contains("tchau") || message.toLowerCase().contains("adeus")|| message.toLowerCase().contains("obrigado"))
			sendDM(userId, m.getAnswer3());
		else
			defaultAnswer(userId);
	}

	private void sendDM(Long userId, String message) throws TwitterException {
		System.out.println("Envia> " + message + " para: " + userId);
		TwitterFactory.getSingleton().sendDirectMessage(userId, message);
	}

	private void defaultAnswer(Long senderId) {
		try {
			sendDM(senderId, m.getMsgDefault());
		} catch (TwitterException e) {
			e.printStackTrace();
		}
	}
}
