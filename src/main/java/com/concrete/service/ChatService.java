package com.concrete.service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concrete.bean.ConnectionBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.endpoint.UserstreamEndpoint;
import com.twitter.hbc.core.processor.LineStringProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

import twitter4j.DirectMessage;
import twitter4j.TwitterObjectFactory;

@Service
public class ChatService {

	@Autowired
	ConnectionBean c;

	ObjectMapper jsonParser = new ObjectMapper();

	@Autowired
	BotService botService;

	@PostConstruct
	public void initRun(){
		while(true){
			try{
				run();
			}catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public void run() throws InterruptedException {
		BlockingQueue<String> queue = new LinkedBlockingQueue<String>(10000);
		UserstreamEndpoint endpoint = new UserstreamEndpoint();
		// add some track terms
		endpoint.addQueryParameter(Constants.WITH_PARAM, Constants.WITH_FOLLOWINGS);

		Authentication auth = new OAuth1(c.getConsumerKey(), c.getConsumerSecret(), c.getUserToken(), c.getUserSecret());

		// Create a new BasicClient. By default gzip is enabled.
		Client client = new ClientBuilder()
				.hosts(Constants.USERSTREAM_HOST)
				.endpoint(endpoint)
				.authentication(auth)
				.processor(new LineStringProcessor(queue))
				.build();

		// Establish a connection
		client.connect();

		jsonParser.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
		// Do whatever needs to be done with messages
		while (true) {
			String msg = queue.take();
			System.out.println(msg);
			try{
				DirectMessage dm = (DirectMessage) TwitterObjectFactory.createObject(msg);

				if(c.getUserId() != dm.getSenderId())
					botService.sendChatMessage(dm.getSenderId(), dm.getText());
				else if(c.getUserId() == dm.getSenderId() && "exit_bot".equals(dm.getText()))
					break;
			}catch (Exception e) {}
		}

		client.stop();
	}
}
