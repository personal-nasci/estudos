package com.concrete.bean;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConnectionBean {

	@Value("${oauth.consumerKey}")
	private String consumerKey;
	
	@Value("${oauth.consumerSecret}")
	private String consumerSecret; 
	
	@Value("${oauth.accessToken}")
	private String userToken; 
	
	@Value("${oauth.accessTokenSecret}")
	private String userSecret;
	
	@Value("${user.id}")
	private Long userId;

	@Value("${user.screenName}")
	private String userName;
	
	public String getConsumerKey() {
		return consumerKey;
	}

	public String getConsumerSecret() {
		return consumerSecret;
	}

	public String getUserSecret() {
		return userSecret;
	}

	public String getUserToken() {
		return userToken;
	}

	public Long getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

}
