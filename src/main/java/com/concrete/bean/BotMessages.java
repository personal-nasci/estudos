package com.concrete.bean;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BotMessages {

	@Value("${message.default}")
	private String msgDefault;
	
	@Value("${message.question1}")
	private String question1;
	
	@Value("${message.answer1}")
	private String answer1;
	
	@Value("${message.answer2}")
	private String answer2;
	
	@Value("${message.answer3}")
	private String answer3;
			
	@Value("${message.understand}")
	private String understand;
	
	@Value("${message.goodBye}")
	private String bye;

	public String getMsgDefault() {
		return msgDefault;
	}

	public String getQuestion1() {
		return question1;
	}

	public String getUnderstand() {
		return understand;
	}

	
	public String getAnswer1() {
		return answer1;
	}

	public String getAnswer2() {
		return answer2;
	}

	public String getAnswer3() {
		return answer3;
	}

	public String getBye() {
		return bye;
	}

	
}
